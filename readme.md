# Context

This project has been created to demonstrate how properly use data collected from widgets and consent notice.

You can collect consent for purposes and choices for preferences with Didomi products. 

For both operations, you will need to:
- authenticate your backend / frontend to our APIs
- request our Consents API GET /consents/users?organization_id=ADD_YOURS&organization_user_id=ADD_YOURS

For purpose:
- use our methods `isConsentGiven` to know if consent has been given (`true` or `false`)
- store result of `isConsentGiven` in your frontend to condition blocks, ads, sections, etc...

For preference:
- use our methods `hasValueBeenSelectedForPreference` to know if choice of a preference has been selected (`true` or `false`)
- store result of `hasValueBeenSelectedForPreference` in your frontend to condition blocks, ads, sections, etc...

# Start the project

```shell
open ./index.html
```

# Demo

https://www.loom.com/share/0598f289929348c08223abe33f46c74c

