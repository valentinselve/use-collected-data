const submitButton = document.getElementById("submit")
const loginButton = document.getElementById("login")

const LOGIN_HEADERS =  new Headers({
    'Content-Type': 'application/json',
    'cache': 'no-cache',
})

async function login (key, secret) {
    const body = JSON.stringify({
        type: 'api-key',
        key: key,
        secret: secret
    })
    const credentialsRequest = await fetch(`https://api.didomi.io/sessions`, {
        headers: LOGIN_HEADERS,
        method: 'POST',
        mode: 'cors',
        body
    })
    const accessFetched = await credentialsRequest.json()
    window.localStorage.setItem('token', accessFetched.access_token)
    alert('Logged successfully')
}

async function fetchConsents (organizationId, organizationUserId) {
    const token = window.localStorage.getItem('token')
    if (!token) {
        alert('you need to login first')
        return;
    }
    const headers = new Headers({
        'Content-Type': 'application/json',
        'cache': 'no-cache',
        'Authorization': `Bearer ${token}`
    })
    const consentGivenUrl = `https://api.didomi.io/consents/users?organization_id=${organizationId}&organization_user_id=${organizationUserId}`
    const consentGivenRequest = await fetch(consentGivenUrl, { headers })
    const consentResponse = await consentGivenRequest.json()
    return consentResponse.data[0].consents
}

async function isConsentGiven (consents, purposeId) {
    return consents.purposes.find(purpose => purpose.id == purposeId).enabled
}

async function hasValueBeenSelectedForPreference (consents, preferenceId, valueId) {
    const { purposes } = consents
    const hasValueBeenSelected = purposes.some(purpose => {
        const preferenceValue = purpose.values[preferenceId]
        if (preferenceValue) {
            const valuesSelected = preferenceValue.value.split(',')
            return valuesSelected.includes(valueId)
        }
        return false
    })
    return hasValueBeenSelected
}

const appendDiv = (content) => {
    const currentDiv = document.getElementById("consent-status")
    const newDiv = document.createElement('div')
    const newContent = document.createTextNode(content)
    const newDivider = document.createElement('hr')        
    newDiv.appendChild(newContent)
    currentDiv.appendChild(newDiv, newDivider)
}

function getFormData() {
    const organizationId = document.getElementById("organization_id").value
    const organizationUserId = document.getElementById("organization_user_id").value
    const purposeId = document.getElementById("purpose_id").value
    const preferenceId = document.getElementById("preference_id").value
    const valueId = document.getElementById("value_id").value
    return { organizationId, organizationUserId, purposeId, preferenceId, valueId }
}

submitButton.addEventListener('click', async () => {
    const { organizationId, organizationUserId, purposeId, preferenceId, valueId } = getFormData()
    const consents = await fetchConsents(organizationId, organizationUserId)
    if (purposeId != null) {
        const consentStatus = await isConsentGiven(consents, purposeId)
        appendDiv(`Consent to purpose (${purposeId}) has ${consentStatus === true ? '' : 'not'} been given.`)
    }

    if (preferenceId && valueId) {
        const preferenceStatus = await hasValueBeenSelectedForPreference(consents, preferenceId, valueId)
        appendDiv(`Value (${valueId}) of selectedPreference (${preferenceId}) has ${preferenceStatus === true ? '' : 'not'} been selected.`)
    }
})

loginButton.addEventListener('click', () => {
    key = document.getElementById("key").value
    secret = document.getElementById("secret").value
    login(key, secret)
})